# lilly

![](https://img.shields.io/badge/written%20in-C%2B%2B-blue)

An open-source clone of Billy, a popular lightweight music player.

Lilly is "a lightweighted no nonsense audio player" for taking control of your music and getting it out of your way. No skins or tags.

It is an attempt at a pixel-perfect recreation of Sheep's fantastic Billy player ( http://www.sheepfriends.com/?page=billy ), of which I am a huge fan and have been using exclusively since 2004. However, there has been precious little development on that project since, and in hindsight it has poor support for unicode and audio formats.

Features
- Full unicode support
- MP3, OGG, FLAC, AC3, AAC, M4A and ALAC support
- Global hotkeys

Still to come
There are a few things missing before attaining feature parity with Billy, but they'll be ready soon.
- Tray icon
- Support for scrobbling to Last.fm
- Support for playing streaming internet radio
- Extra keyboard shortcuts
- Extra tools
- Favorite playlists

As well as feature-parity, there's the possibility of going above and beyond;
- Remote control interface
- Cross-platform
- Extra customisation options

Original project page: https://code.google.com/p/lilly-player/


## Download

- [⬇️ lilly-beta-0.4-src.zip](dist-archive/lilly-beta-0.4-src.zip) *(87.65 KiB)*
- [⬇️ lilly-beta-0.4-bin.zip](dist-archive/lilly-beta-0.4-bin.zip) *(329.44 KiB)*
- [⬇️ lilly-beta-0.3-src.zip](dist-archive/lilly-beta-0.3-src.zip) *(76.27 KiB)*
- [⬇️ lilly-beta-0.3-bin.7z](dist-archive/lilly-beta-0.3-bin.7z) *(320.31 KiB)*
- [⬇️ lilly-beta-0.2-src.7z](dist-archive/lilly-beta-0.2-src.7z) *(27.25 KiB)*
- [⬇️ lilly-beta-0.2-bin.zip](dist-archive/lilly-beta-0.2-bin.zip) *(318.67 KiB)*
- [⬇️ lilly-beta-0.1-bin.7z](dist-archive/lilly-beta-0.1-bin.7z) *(310.08 KiB)*
